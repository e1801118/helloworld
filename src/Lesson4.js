//Esimerkki miten komponetti tehdään
function Lesson4(props){
    const items = [];
    for (let i = 99; i > 0; i-=3) {
        items.push(<li>{i}</li>);
      }
    
    return(
        <div>
            <hr></hr>
            Lesson 4 master null
            {props.json.title}
            {props.json.email}
            {props.json.phone}
            <ul>{items}</ul>
        </div>
    );
}
export default Lesson4;

/*import React from "react";

function App() {
  const users = ["user1", "user2", "user3"];
  const final = [];

  for (let  user of users) {
    final.push(<li key={user}>{user}</li>);
  }
  return (
    <div className="App">
      <ul>{final}</ul>
    </div>
  );
}

export default App;*/