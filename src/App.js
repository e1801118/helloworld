import logo from './logo.svg';
import './App.css';
import Lesson2 from './Lesson2';
import Lesson3 from './Lesson3';
import Lesson4 from './Lesson4';

function App() {
  const secondName="DDR-15";
  let json = {}; //Make Json object
  json.title = "Json Object";
  json.email = "11@11.com";
  json.phone = "12-12-12-12-12";
  return (
    <div className="App">
      Welcome
      <Lesson2/>
      <Lesson3 name="Master" familyName="KEK" second={secondName} email={json.email}>asdasd</Lesson3>
      <Lesson4 json={json}/>
    </div>
  );
}

export default App;
