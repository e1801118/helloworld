//Esimerkki miten komponetti tehdään
import React from "react";
function Lesson3(props){
    return(
        <div>
            <hr></hr>
            Lesson 3 <br></br>
            {props.name} {props.second}
            <br></br>
            {props.familyName}
            {props.email}
        </div>
    )
}
export default Lesson3;